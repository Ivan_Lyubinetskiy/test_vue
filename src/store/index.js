import Vue from 'vue'
import Vuex from 'vuex'
import information from './modules/informations'
import toggle from './modules/toggle'
import common from './modules/common'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    information,
    toggle,
    common
  }
})