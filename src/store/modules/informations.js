import * as fb from 'firebase'
import axios from 'axios'

class Info {
  constructor(accountNumber, sum, numberCard, nameCard, cvv, date) {
    this.accountNumber = accountNumber
    this.sum = sum
    this.numberCard = numberCard
    this.nameCard = nameCard
    this.cvv = cvv
    this.date = date
  }
}

export default {
  state: {
    informations: []
  },
  mutations: {
    setPayment: (state, payload) => {
      state.informations.push(payload)
    },
    getPayment: (state, payload) => {
      state.informations = payload
    }
  },
  actions: {
    setPayment: ({
      commit
    }, payload) => {
      const newInfo = new Info(
        payload.accountNumber,
        payload.sum,
        payload.numberCard,
        payload.nameCard,
        payload.cvv,
        payload.date
      )
      axios.post('https://testingtask-2a3c4.firebaseio.com/informations.json', newInfo)
        .then(response => {
          commit('clearError')
          commit('setLoading', true)
        })
        .catch(error => {
          commit('setError', error.massage)
          commit('setLoading', false)
          console.log(error);
        })
    },
    getPayment: ({
      commit
    }, payload) => {
      const informations = []
      axios.get('https://testingtask-2a3c4.firebaseio.com/informations.json')
        .then(response => {
          const data = response.data
          const resultData = []
          Object.keys(data).forEach(key => {
            const inf = data[key]
            resultData.push(
              new Info(
                inf.accountNumber,
                inf.sum,
                inf.numberCard,
                inf.nameCard,
                inf.cvv,
                inf.date
              )
            )
            commit('getPayment', resultData)
          })
        })
        .catch(error => {
          console.log(error);
        })
    }
  },
  getters: {
    informations(state) {
      return state.informations
    }
  }
}