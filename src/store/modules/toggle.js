class Tog {
  constructor(toggle) {
    this.toggle = toggle
  }
}

export default {
  state: {
    toggle: false
  },
  mutations: {
    setToggle: (state, payload) => {
      state.toggle = payload
    }
  },
  actions: {
    setToggle({
      commit
    }, payload) {
      commit('setToggle', payload)
    }
  },
  getters: {
    toggle(state) {
      return state.toggle
    }
  }
}