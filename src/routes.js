import Vue from 'vue'
import Router from 'vue-router'
import Payment from './pages/Payment'
import Success from './pages/Success'
import History from './pages/History'
import store from './store/modules/toggle'

Vue.use(Router)

export default new Router({
  routes: [{
      path: '/',
      name: 'payment',
      component: Payment,
      props: true
    },
    {
      path: '/success',
      name: 'success',
      component: Success,
      beforeEnter(to, from, next) {
        if (store.state.toggle) {
          next()
        } else {
          next('/')
        }
      }
    },
    {
      path: '/history',
      name: 'history',
      component: History
    }
  ],
  mode: 'history'
})