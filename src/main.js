import Vue from 'vue'
import App from './App.vue'
import VeeValidate from 'vee-validate';
import router from './routes'
import store from './store'
import * as fb from 'firebase/app'

Vue.use(VeeValidate)

new Vue({
  el: '#app',
  store,
  render: h => h(App),
  router,
  created() {
    var firebaseConfig = {
      apiKey: 'AIzaSyAJxvki8sOkcXKeL6deAVZLZcot4f8EeXs',
      authDomain: 'testingtask-2a3c4.firebaseapp.com',
      databaseURL: 'https://testingtask-2a3c4.firebaseio.com',
      projectId: 'testingtask-2a3c4',
      storageBucket: 'testingtask-2a3c4.appspot.com',
      messagingSenderId: '193767244240',
      appId: '1:193767244240:web:b44b266fe510f712'
    };
    // Initialize Firebase
    fb.initializeApp(firebaseConfig),
      this.$store.dispatch('getPayment')
  }
})